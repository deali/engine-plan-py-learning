# question 2
list1 = [1, 2, 3, 4, 5, 6, 7]
list2 = list([1, 2, 3, 4, 5, 6])
print('list1', list1)
print('list2', list2)

# question 3
print()
print('list1 elem1', list1[0])
list1[0] = 100
print('list1 elem1', list1[0])

# question 4
print()
for item in list1:
    print(item)
list3 = [e + 1 for e in list2]
print(list3)

# question 5
print()
print('length of list1', len(list1))
list4 = list1 + list2
print(list4)
list1.extend(list2)
print(list1)

# question 6 切片
print()
# 取第二到第五个元素
print(list2[1:5])
# 取前五个元素
print(list2[:-1])
# 列表逆序
print(list2[::-1])

# question 7
print()
import random
list5 = [random.randint(1, 10) for _ in range(5)]
print(list5)
list6 = [e for e in list5 if e % 2 == 0]
print(list6)
